# Erlang IRC
Internet relay chat implemented in Erlang

## Documentation
This implementation is derived from 
[RFC 1459](https://tools.ietf.org/html/rfc1459.html) and
the [Modern IRC docs](https://modern.ircdocs.horse/), with the 
following alterations and interpretations:

- _nothing yet_

## License 
This software and associated documentation files are licensed under the
[MIT License](https://opensource.org/licenses/MIT).

This file may not be copied, modified, or distributed except according to
those terms.

